package main

//Key -
type Key struct {
	Applicable
	Respawnable
	respawnRoom *Room
}

//NewKey -
func NewKey() *Key {
	return &Key{}
}

func (k *Key) String() string {
	return "ключи"
}

//Name -
func (k *Key) Name() string {
	return k.String()
}

//Apply -
func (k *Key) Apply(i Instrumentable) string {
	return i.apply(k)
}

func (k *Key) pick(i Inventory) {
	return
}

func (k *Key) throw(i Inventory) {
	return
}

//SetRespawnRoom -
func (k *Key) SetRespawnRoom(r *Room) {
	k.respawnRoom = r
}

//Respawn -
func (k *Key) Respawn() {
	k.respawnRoom.AddItem(k)
}
