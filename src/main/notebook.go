package main

//Notebook -
type Notebook struct {
	Pickable
	Respawnable
	respawnRoom *Room
}

//NewNotebook -
func NewNotebook() *Notebook {
	return &Notebook{}
}

//String -
func (n *Notebook) String() string {
	return "конспекты"
}

//Name -
func (n *Notebook) Name() string {
	return n.String()
}

func (n *Notebook) pick(i Inventory) {
	return
}

func (n *Notebook) throw(i Inventory) {
	return
}

//SetRespawnRoom -
func (n *Notebook) SetRespawnRoom(r *Room) {
	n.respawnRoom = r
}

//Respawn -
func (n *Notebook) Respawn() {
	n.respawnRoom.AddItem(n)
}
