package main

//HasName -
type HasName interface {
	Name() string
}

//Respawnable -
type Respawnable interface {
	Respawn()
}

//Applicable -
type Applicable interface {
	HasName
	Apply(i Instrumentable) string
}

//Instrumentable -
type Instrumentable interface {
	HasName
	apply(a Applicable) string
}

//Visitable -
type Visitable interface {
	HasName
	isAvailableFor(p *Player) (bool, string)
	visit(p *Player) *Room
	leave(p *Player)
}

//Wearable -
type Wearable interface {
	HasName
	Respawnable
	wear(h Hanger)
	takeOff(h Hanger)
}

//Pickable -
type Pickable interface {
	HasName
	Respawnable
	pick(i Inventory)
	throw(i Inventory)
}

//Container -
type Container interface {
	HasName
	Add(thing HasName)
	Has(name string) bool
	Get(name string) HasName
	Extract(name string) HasName
	Content() []string
	Empty() bool
	Size() int
}

//Inventory -
type Inventory interface {
	HasName
	AddItem(p Pickable)
	HasItem(name string) bool
	ExtractItem(name string) Pickable
}

//Hanger -
type Hanger interface {
	HasName
	Hang(w Wearable)
	HasWear(name string) bool
	TakeOff(name string) Wearable
}
