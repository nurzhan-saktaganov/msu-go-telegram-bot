package main

import (
	"bytes"
	"time"
)

const adminID = 210524742
const kickoutTime = 15 * time.Minute

var defaultRoom Visitable
var players map[int]*Player
var timers map[int]*time.Timer

func init() {
	initGame()
}

func addPlayer(p *Player) {
	p.room = defaultRoom.visit(p)
}

func initGame() {
	players = make(map[int]*Player)
	timers = make(map[int]*time.Timer)
	initGameWorld()
}

func initGameWorld() {
	kitchen := NewRoom("кухня")
	defaultRoom = kitchen
	kitchen.SetOnGreet(
		func(p *Player) string {
			var buffer bytes.Buffer
			buffer.WriteString("ты находишься на кухне, на столе чай, надо")
			if !p.hasWearing("рюкзак") {
				buffer.WriteString(" собрать рюкзак и")
			}
			buffer.WriteString(" идти в универ")
			return buffer.String()
		})
	kitchen.SetWelcomeMessage("кухня, ничего интересного")

	hallway := NewRoom("коридор")
	hallway.SetWelcomeMessage("ничего интересного")

	room := NewRoom("комната")
	room.SetWelcomeMessage("ты в своей комнате")
	room.SetEmptyRoomMessage("пустая комната")
	room.SetPickableLocation("на столе: ")
	room.SetWearingLocation("на стуле - ")

	backpack := NewBackpack()
	backpack.SetRespawnRoom(room)
	room.Hang(backpack)
	key := NewKey()
	key.SetRespawnRoom(room)
	room.AddItem(key)
	notebook := NewNotebook()
	notebook.SetRespawnRoom(room)
	room.AddItem(notebook)

	ConnectRooms(hallway, kitchen)
	ConnectRooms(hallway, room)

	street := NewRoom("улицe")
	street.SetWelcomeMessage("на улице весна")

	home := NewLocation("домой")
	outdoor := NewLocation("улица")

	home.setEntry(hallway)
	outdoor.setEntry(street)
	ConnectLocations(home, outdoor)
}
