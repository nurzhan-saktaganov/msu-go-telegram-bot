package main

import (
	"fmt"
	"strings"
)

//Player -
type Player struct {
	Hanger
	hanger        map[string]Wearable
	name          string
	userID        int
	room          *Room
	commandResult chan string
}

//NewPlayer -
func NewPlayer(name string, userID int) *Player {
	return &Player{
		name:          name,
		userID:        userID,
		hanger:        make(map[string]Wearable),
		commandResult: make(chan string),
	}
}

var handleInput map[string]func(p *Player, params []string) string

func init() {
	handleInput = make(map[string]func(p *Player, params []string) string)

	handleInput["осмотреться"] = func(p *Player, params []string) string {
		commandResult := p.LookAround()
		return commandResult
	}

	handleInput["идти"] = func(p *Player, params []string) string {
		var commandResult string
		if len(params) < 2 {
			commandResult = "неправильная команда"
		} else {
			commandResult = p.Go(params[1])
		}
		return commandResult
	}

	handleInput["одеть"] = func(p *Player, params []string) string {
		var commandResult string
		if len(params) < 2 {
			commandResult = "неправильная команда"
		} else {
			commandResult = p.Wear(params[1])
		}
		return commandResult
	}

	handleInput["взять"] = func(p *Player, params []string) string {
		var commandResult string
		if len(params) < 2 {
			commandResult = "неправильная команда"
		} else {
			commandResult = p.Pick(params[1])
		}
		return commandResult
	}

	handleInput["применить"] = func(p *Player, params []string) string {
		var commandResult string
		if len(params) < 3 {
			commandResult = "неправильная команда"
		} else {
			commandResult = p.Apply(params[1], params[2])
		}
		return commandResult
	}

	handleInput["сказать"] = func(p *Player, params []string) string {
		var commandResult string
		if len(params) < 2 {
			commandResult = "неправильная команда"
		} else {
			messageText := strings.Join(params[1:], " ")
			commandResult = p.BroadcastMessage(messageText)
		}
		return commandResult
	}

	handleInput["сказать_игроку"] = func(p *Player, params []string) string {
		var commandResult string
		if len(params) < 2 {
			commandResult = "неправильная команда"
		} else {
			var messageText string
			if len(params) == 2 {
				messageText = ""
			} else {
				messageText = strings.Join(params[2:], " ")
			}
			commandResult = p.PrivateMessage(params[1], messageText)
		}
		return commandResult
	}

	handleInput["сброс"] = func(p *Player, params []string) string {
		var commandResult string
		if p.userID != adminID {
			commandResult = "Команда доступна только администратору"
		} else {
			commandResult = "Перезапуск игры"
			for _, timer := range timers {
				timer.Stop()
			}
			for _, player := range players {
				player.OnDelete()
			}
			initGame()
		}
		return commandResult
	}
}

//String -
func (p *Player) String() string {
	return "игрок"
}

//Name -
func (p *Player) Name() string {
	return p.name
}

//Go -
func (p *Player) Go(name string) string {
	if !p.room.HasNeighbor(name) {
		return fmt.Sprintf("нет пути в %s", name)
	}
	visitable := p.room.GetNeighbor(name)
	ok, why := visitable.isAvailableFor(p)
	if !ok {
		return why
	}
	p.room.leave(p)
	p.room = visitable.visit(p)
	return p.room.Welcome()
}

//LookAround -
func (p *Player) LookAround() string {
	return p.room.Greet(p)
}

//Wear -
func (p *Player) Wear(name string) string {
	if !p.room.HasWear(name) {
		return fmt.Sprintf("тут нет: %s", name)
	}
	w := p.room.TakeOff(name)
	w.wear(p)
	p.hanger[w.Name()] = w
	return fmt.Sprintf("вы одели: %s", name)
}

//Pick -
func (p *Player) Pick(name string) string {
	if !p.room.HasItem(name) {
		return "нет такого"
	}

	var inventory Inventory
	var ok bool
	for _, v := range p.hanger {
		inventory, ok = v.(Inventory)
		if ok {
			break
		}
	}
	if !ok {
		return "некуда класть"
	}
	pickable := p.room.ExtractItem(name)
	inventory.AddItem(pickable)
	return fmt.Sprintf("предмет добавлен в инвентарь: %s", name)
}

func (p *Player) hasItem(name string) bool {
	for _, v := range p.hanger {
		inventory, ok := v.(Inventory)
		if !ok {
			continue
		}
		if inventory.HasItem(name) {
			return true
		}
	}
	return false
}

func (p *Player) getApplicableItem(name string) (Applicable, bool) {
	for _, w := range p.hanger {
		inventory, ok := w.(Inventory)
		if !ok {
			continue
		}
		if !inventory.HasItem(name) {
			continue
		}
		item := inventory.ExtractItem(name)
		applicable, ok := item.(Applicable)
		inventory.AddItem(item)
		if ok {
			return applicable, true
		}
	}
	return nil, false
}

//Apply -
func (p *Player) Apply(what string, to string) string {
	applicable, ok := p.getApplicableItem(what)
	if !ok {
		return fmt.Sprintf("нет предмета в инвентаре - %s", what)
	}
	if !p.room.HasInstrumentable(to) {
		return "не к чему применить"
	}
	instrumentable := p.room.GetInstrumentable(to)
	return applicable.Apply(instrumentable)
}

func (p *Player) hasWearing(name string) bool {
	_, ok := p.hanger[name]
	return ok
}

//HandleInput -
func (p *Player) HandleInput(command string) string {
	splitted := strings.Split(command, " ")
	if len(splitted) == 0 {
		return "неправильная команда"
	}
	commandName := splitted[0]
	if _, ok := handleInput[commandName]; !ok {
		return "неизвестная команда"
	}
	return handleInput[commandName](p, splitted)
}

//HandleOutput -
func (p *Player) HandleOutput(output string) {
	p.commandResult <- output
}

//GetOutput -
func (p *Player) GetOutput() <-chan string {
	return p.commandResult
}

//BroadcastMessage -
func (p *Player) BroadcastMessage(msg string) string {
	p.room.broadcastMessage(p, msg)
	return ""
}

//PrivateMessage -
func (p *Player) PrivateMessage(to string, msg string) string {
	success := p.room.privateMessage(p, to, msg)
	if success {
		return ""
	}
	return "тут нет такого игрока"
}

//OnDelete -
func (p *Player) OnDelete() {
	defer close(p.commandResult)
	defer p.room.leave(p)
	if len(p.hanger) == 0 {
		return
	}
	for _, w := range p.hanger {
		respawnable, ok := w.(Respawnable)
		if !ok {
			return
		}
		respawnable.Respawn()
	}
}
