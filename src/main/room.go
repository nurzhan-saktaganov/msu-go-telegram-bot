package main

import (
	"bytes"
	"fmt"
	"strings"
)

//TODO повесить mutex для предотвращения конкурентного доступа
//TODO поменять методы, которые меняют состояние объекта
//TODO на наподобие object.Method(...) (result string, success bool)

type onGreetFunc func(p *Player) string

//Room -
type Room struct {
	Hanger
	Inventory
	Visitable
	name             string
	hanger           map[string]Wearable
	inventory        map[string]Pickable
	instrumentable   map[string]Instrumentable
	guests           map[string]*Player
	neighbors        map[string]Visitable
	hangerPrefix     string
	inventoryPrefix  string
	onGreet          onGreetFunc
	welcomeMessage   string
	emtpyRoomMessage string
	canGoMessage     string
}

//NewRoom -
func NewRoom(name string) *Room {
	return &Room{
		name:           name,
		hanger:         make(map[string]Wearable),
		inventory:      make(map[string]Pickable),
		instrumentable: make(map[string]Instrumentable),
		guests:         make(map[string]*Player),
		neighbors:      make(map[string]Visitable),
		onGreet:        func(p *Player) string { return "" },
	}
}

//String -
func (r *Room) String() string {
	return "комната"
}

//Name -
func (r *Room) Name() string {
	return r.name
}

//AddItem -
func (r *Room) AddItem(p Pickable) {
	p.pick(r)
	r.inventory[p.Name()] = p
}

//HasItem -
func (r *Room) HasItem(name string) bool {
	_, ok := r.inventory[name]
	return ok
}

//ExtractItem -
func (r *Room) ExtractItem(name string) Pickable {
	p := r.inventory[name]
	delete(r.inventory, name)
	p.throw(r)
	return p
}

//Hang -
func (r *Room) Hang(w Wearable) {
	w.wear(r)
	r.hanger[w.Name()] = w
}

//HasWear -
func (r *Room) HasWear(name string) bool {
	_, ok := r.hanger[name]
	return ok
}

//TakeOff -
func (r *Room) TakeOff(name string) Wearable {
	w := r.hanger[name]
	delete(r.hanger, name)
	w.takeOff(r)
	return w
}

func (r *Room) isAvailableFor(p *Player) (bool, string) {
	return true, ""
}

func (r *Room) leave(p *Player) {
	delete(r.guests, p.Name())
}

func (r *Room) visit(p *Player) *Room {
	r.guests[p.Name()] = p
	return r
}

//AddInstrumentable -
func (r *Room) AddInstrumentable(i Instrumentable) {
	r.instrumentable[i.Name()] = i
}

//HasInstrumentable -
func (r *Room) HasInstrumentable(name string) bool {
	_, ok := r.instrumentable[name]
	return ok
}

//GetInstrumentable -
func (r *Room) GetInstrumentable(name string) Instrumentable {
	return r.instrumentable[name]
}

//SetOnGreet -
func (r *Room) SetOnGreet(f onGreetFunc) {
	r.onGreet = f
}

//SetWelcomeMessage -
func (r *Room) SetWelcomeMessage(message string) {
	r.welcomeMessage = message
}

//SetEmptyRoomMessage -
func (r *Room) SetEmptyRoomMessage(message string) {
	r.emtpyRoomMessage = message
}

//SetWearingLocation -
func (r *Room) SetWearingLocation(where string) {
	r.hangerPrefix = where
}

//SetPickableLocation -
func (r *Room) SetPickableLocation(where string) {
	r.inventoryPrefix = where
}

//ConnectRooms -
func ConnectRooms(r1 *Room, r2 *Room) error {
	r1.AddNeighbor(r2)
	r2.AddNeighbor(r1)
	return nil
}

//AddNeighbor -
func (r *Room) AddNeighbor(v Visitable) {
	name := v.Name()
	r.neighbors[name] = v
	if r.canGoMessage == "" {
		r.canGoMessage = fmt.Sprintf("можно пройти - %s", name)
	} else {
		r.canGoMessage += fmt.Sprintf(", %s", name)
	}
	return
}

//Greet -
func (r *Room) Greet(p *Player) string {
	greeting := make([]string, 0, 4)
	itemsNames := r.content()
	greetingMessage := r.onGreet(p)
	guestsNames := r.guestsList(p)
	if greetingMessage != "" {
		greeting = append(greeting, greetingMessage)
	}
	if itemsNames != "" {
		greeting = append(greeting, itemsNames)
	}
	if r.canGoMessage != "" {
		greeting = append(greeting, r.canGoMessage)
	}
	if guestsNames != "" {
		greeting = append(greeting, guestsNames)
	}
	return strings.Join(greeting, ". ")
}

//content
func (r *Room) content() string {
	if len(r.hanger) == 0 && len(r.inventory) == 0 {
		return r.emtpyRoomMessage
	}

	content := make([]string, 0, 2)
	contentPickable := r.contentPickable()
	contentWearable := r.contentWearable()

	if contentPickable != "" {
		content = append(content, contentPickable)
	}
	if contentWearable != "" {
		content = append(content, contentWearable)
	}
	return strings.Join(content, ", ")
}

func (r *Room) contentPickable() string {
	if len(r.inventory) == 0 {
		return ""
	}
	var buffer bytes.Buffer
	var names []string

	for k := range r.inventory {
		names = append(names, k)
	}

	buffer.WriteString(r.inventoryPrefix)
	buffer.WriteString(strings.Join(names, ", "))
	return buffer.String()
}

func (r *Room) contentWearable() string {
	if len(r.hanger) == 0 {
		return ""
	}
	var buffer bytes.Buffer
	var names []string

	for k := range r.inventory {
		names = append(names, k)
	}

	buffer.WriteString(r.hangerPrefix)
	buffer.WriteString(strings.Join(names, ", "))
	return buffer.String()
}

func (r *Room) guestsList(p *Player) string {
	var buffer bytes.Buffer
	var names []string

	name := p.Name()

	for k := range r.guests {
		if k == name {
			continue
		}
		names = append(names, k)
	}

	if len(names) == 0 {
		return ""
	}

	buffer.WriteString("Кроме вас тут ещё ")
	buffer.WriteString(strings.Join(names, ", "))
	return buffer.String()
}

//Welcome -
func (r *Room) Welcome() string {
	var buffer bytes.Buffer
	buffer.WriteString(r.welcomeMessage)
	buffer.WriteString(". ")
	buffer.WriteString(r.canGoMessage)
	return buffer.String()
}

//HasNeighbor -
func (r *Room) HasNeighbor(name string) bool {
	_, ok := r.neighbors[name]
	return ok
}

//GetNeighbor -
func (r *Room) GetNeighbor(name string) Visitable {
	return r.neighbors[name]
}

func (r *Room) broadcastMessage(p *Player, message string) {
	message = fmt.Sprintf("%s говорит: %s", p.Name(), message)
	for _, guest := range r.guests {
		guest.HandleOutput(message)
	}
}

func (r *Room) privateMessage(p *Player, to, message string) bool {
	receiver, ok := r.guests[to]
	if !ok {
		return false
	}
	if message != "" {
		message = fmt.Sprintf("%s говорит вам: %s", p.Name(), message)
	} else {
		message = fmt.Sprintf("%s выразительно молчит, смотря на вас", p.Name())
	}
	receiver.HandleOutput(message)
	return true
}
