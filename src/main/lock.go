package main

//Lock -
type Lock struct {
	Instrumentable
	open bool
}

//NewLock -
func NewLock() *Lock {
	return &Lock{
		open: false,
	}
}

//
func (l *Lock) String() string {
	return "дверь"
}

//Name -
func (l *Lock) Name() string {
	return l.String()
}

func (l *Lock) apply(a Applicable) string {
	_, ok := a.(*Key)
	if !ok {
		return "открывать надо ключом"
	}
	l.open = !l.open
	return l.status()
}

func (l *Lock) isOpen() bool {
	return l.open
}

func (l *Lock) status() string {
	if l.open {
		return "дверь открыта"
	}
	return "дверь закрыта"
}
