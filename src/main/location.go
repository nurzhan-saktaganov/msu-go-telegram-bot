package main

//Location -
type Location struct {
	Visitable
	name  string
	entry *Room
	lock  *Lock
}

//NewLocation -
func NewLocation(name string) *Location {
	return &Location{
		name: name,
	}
}

//Name -
func (l *Location) Name() string {
	return l.name
}

func (l *Location) isAvailableFor(p *Player) (bool, string) {
	var reason string
	if !l.lock.isOpen() {
		reason = "дверь закрыта"
	}
	return l.lock.isOpen(), reason
}

func (l *Location) visit(p *Player) *Room {
	return l.entry.visit(p)
}

func (l *Location) leave(p *Player) {
	l.entry.leave(p)
}

func (l *Location) setEntry(r *Room) {
	l.entry = r
}

func (l *Location) setLock(lock *Lock) {
	l.lock = lock
}

//ConnectLocations -
func ConnectLocations(l1 *Location, l2 *Location) {
	lock := NewLock()
	l1.lock, l2.lock = lock, lock
	l1.entry.AddNeighbor(l2)
	l2.entry.AddNeighbor(l1)
	l1.entry.AddInstrumentable(lock)
	l2.entry.AddInstrumentable(lock)
}
