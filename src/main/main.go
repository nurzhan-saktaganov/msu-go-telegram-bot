package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

// для вендоринга используется GB
// сборка проекта осуществляется с помощью gb build
// установка зависимостей - gb vendor fetch gopkg.in/telegram-bot-api.v4
// установка зависимостей из манифеста - gb vendor restore

//var buttons = []tgbotapi.KeyboardButton{
//	tgbotapi.KeyboardButton{Text: "Get Joke"},
//}

//WebhookURL - При старте приложения, оно скажет телеграму ходить с обновлениями по этому URL
const WebhookURL = "https://msugolangbot.herokuapp.com/bot"

const loginFormTmpl = `
<html>
	<body>
	<form action="/authorize" method="post">
		Login: <input type="text" name="login">
		Password: <input type="password" name="password">
		<input type="submit" value="Login">
	</form>
	</body>
</html>
`

const createTableIfNotExist = `
CREATE TABLE IF NOT EXISTS  "commands" (
  "id" serial NOT NULL,
  "from" text NOT NULL,
  "command" text NOT NULL,
  "result" text NOT NULL,
  "time" integer NOT NULL
  );
`

//UserInfo -
type UserInfo struct {
	name      string
	userAgent string
	timer     *time.Timer
}

var db *sql.DB

var sessions = map[string]*UserInfo{}

func mainPage(w http.ResponseWriter, r *http.Request) {
	sessionID, err := r.Cookie("session_id")
	if err == http.ErrNoCookie {
		w.Write([]byte(loginFormTmpl))
		return
	} else if err != nil {
		PanicOnErr(err)
	}
	userInfo, ok := sessions[sessionID.Value]
	if !ok {
		w.Write([]byte(loginFormTmpl))
		return
	}
	userInfo.timer.Reset(15 * time.Minute)
	sessionID.Expires = time.Now().Add(15 * time.Minute)
	http.SetCookie(w, sessionID)
	fmt.Fprintf(w, "Welcome, %s from %s\r\n", userInfo.name, userInfo.userAgent)

	rows, err := db.Query("SELECT * FROM commands ORDER BY time DESC LIMIT 10")
	if err != nil {
		log.Println(err)
		return
	}
	var id int64
	var from string
	var command string
	var result string
	var timestamp int64
	for rows.Next() {
		_ = rows.Scan(&id, &from, &command, &result, &timestamp)
		fmt.Fprintf(w, "%s, %s, %s, %d\r\n", from, command, result, timestamp)
	}
	rows.Close()
}

func authorize(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	inputLogin := r.Form["login"][0]
	inputPassword := r.Form["password"][0]
	if inputLogin == "******" && inputPassword == "******" {
	} else {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	expiration := time.Now().Add(15 * time.Minute)
	sessionID := RandStringRunes(32)
	sessions[sessionID] = &UserInfo{
		name:      inputLogin,
		userAgent: r.UserAgent(),
		timer: time.AfterFunc(15*time.Minute, func() {
			delete(sessions, sessionID)
		}),
	}
	cookie := http.Cookie{Name: "session_id", Value: sessionID, Expires: expiration}
	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/", http.StatusFound)
}

func main() {
	var err error
	log.SetOutput(os.Stdout)
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	PanicOnErr(err)

	err = db.Ping()
	if err == nil {
		log.Printf("Ping to db success\n")
	}
	PanicOnErr(err)

	_, err = db.Query(createTableIfNotExist)
	if err == nil {
		log.Printf("create table success\n")
	}
	PanicOnErr(err)

	stmt, err := db.Prepare("INSERT INTO commands (\"from\", command, result, time) VALUES($1, $2, $3, $4)")
	if err == nil {
		log.Printf("stmt create success\n")
	}
	PanicOnErr(err)

	// Heroku прокидывает порт для приложения в переменную окружения PORT
	port := os.Getenv("PORT")
	bot, err := tgbotapi.NewBotAPI("******")
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// Устанавливаем вебхук
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		log.Fatal(err)
	}

	updates := bot.ListenForWebhook("/bot")
	http.HandleFunc("/", mainPage)
	http.HandleFunc("/authorize", authorize)
	go http.ListenAndServe(":"+port, nil)

	// получаем все обновления из канала updates
	for update := range updates {
		userID := update.Message.From.ID

		if _, exist := players[userID]; !exist {
			userID := userID
			name := update.Message.From.FirstName
			player := NewPlayer(name, userID)

			go func(bot *tgbotapi.BotAPI, chatID int64, messages <-chan string) {
				for messageText := range messages {
					message := tgbotapi.NewMessage(chatID, messageText)
					bot.Send(message)
				}
			}(bot, update.Message.Chat.ID, player.GetOutput())

			addPlayer(player)

			players[userID] = player

			timers[userID] = time.AfterFunc(kickoutTime, func() {
				players[userID].HandleOutput("Ты удалён из игры по таймауту")
				players[userID].OnDelete()
				delete(players, userID)
				delete(timers, userID)
			})
		}

		timers[userID].Reset(kickoutTime)
		commandResult := players[userID].HandleInput(update.Message.Text)
		if len(players) > 0 {
			players[userID].HandleOutput(commandResult)
		} else {
			message := tgbotapi.NewMessage(update.Message.Chat.ID, commandResult)
			bot.Send(message)
		}
		from := update.Message.From.FirstName
		command := update.Message.Text
		_, err = stmt.Exec(from, command, commandResult, time.Now().Unix())
		if err == nil {
			log.Printf("Success insert")
		}
		PanicOnErr(err)
	}
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

//RandStringRunes -
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

//PanicOnErr panics on error
func PanicOnErr(err error) {
	if err != nil {
		log.Printf("%s\n", err)
	}
}
