package main

//Backpack -
type Backpack struct {
	Inventory
	Wearable
	Respawnable
	inventory   map[string]Pickable
	respawnRoom *Room
}

//NewBackpack -
func NewBackpack() *Backpack {
	return &Backpack{
		inventory: make(map[string]Pickable),
	}
}

//String -
func (b *Backpack) String() string {
	return "рюкзак"
}

//Name -
func (b *Backpack) Name() string {
	return b.String()
}

func (b *Backpack) wear(h Hanger) {
	return
}

func (b *Backpack) takeOff(h Hanger) {
	return
}

//AddItem -
func (b *Backpack) AddItem(p Pickable) {
	p.pick(b)
	b.inventory[p.Name()] = p
}

//HasItem -
func (b *Backpack) HasItem(name string) bool {
	_, ok := b.inventory[name]
	return ok
}

//ExtractItem -
func (b *Backpack) ExtractItem(name string) Pickable {
	p := b.inventory[name]
	delete(b.inventory, name)
	p.throw(b)
	return p
}

//SetRespawnRoom -
func (b *Backpack) SetRespawnRoom(r *Room) {
	b.respawnRoom = r
}

//Respawn -
func (b *Backpack) Respawn() {
	for _, pickable := range b.inventory {
		respawnable, ok := pickable.(Respawnable)
		if ok {
			respawnable.Respawn()
		}
	}
	b.respawnRoom.Hang(b)
}
